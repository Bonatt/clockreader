PATH = 'Data'
SIZE = 256*2
NCHANNELS = 1 # 1, 3
AUGMENTS = ['scaled', 'translated', 'rotated', 'flipped', 'gaussian', 'perspective']
