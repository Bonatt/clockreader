import os
import cv2 as cv
import numpy as np
#from augment import *


### Collect all file paths in and below parent path
def GetAllFiles(path, ext=('jpg','png')): 
  #path = './data/'  == './data' ~= 'data/' == 'data'
  # Get filenames and prepend directory paths
  files = [[dirs+'/'+file for file in files] for \
            dirs, subdirs, files in os.walk(path)]
  # Flatten paths
  files = [item for sublist in files for item in sublist]
  # Only get image files
  files = [file for file in files if file.lower().endswith(ext)]
  return files


### Get class names and number of classes
def GetClasses(path):
  classes = os.listdir(path)
  nclasses = len(classes)
  return classes, nclasses


### Transform 'speedy' to np.array([1.,0.]), e.g. Return dict to use.
def Classes2Labels(path):#, file):
  classes, nclasses = GetClasses(path)
  # Keys are Sentiment scores
  keys = classes
  # Values are np.zeros with 1 at appropriate index
  values = []
  for i in range(nclasses):    
    value = np.zeros(nclasses)
    value[i] = 1    
    values.append(value)
  dict = {}
  for i in range(len(keys)):    
    dict[keys[i]] = values[i]      
  return dict


### Load and resize to size*size image
def LoadImages(path, size, predict=False, nimages=int(1e6), nchannels=3):
  # data = [image, label, name, class]
  # image is array of numbers, label is array representing class name
  # name is filename, and class is speedy, eg.
  # predict=True if you do not want labels, and/or if files are not in subdirs
  data = []
  CLASS2LABEL = Classes2Labels(path)

  # [:nimages] drops last element... Instead of messing with splicing,
  # just make nimages=-1 --> nimages=<somelargenumber>=int(1e6)?
  # nimages just gets the first nimages... no bueno for cnn, but was useful
  # for making an image collage
  for file in GetAllFiles(path)[:nimages]:
    # If image is black and white, no need for three channels
    if nchannels == 1:
      image = cv.imread(file, cv.IMREAD_GRAYSCALE)
    else:
      image = cv.imread(file)
      #print(file, np.shape(image))
      # BGR --> RGB
      # https://stackoverflow.com/questions/39316447/opencv-giving-wrong-color-to-colored-images-on-loading
      image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
    image = cv.resize(image, (size, size)).astype(np.float32)

    # To go from (size, size) to (size, size, nchannels).
    # Implemented to make greyscale images work with this cnn function
    image = np.reshape(image, (size, size, nchannels))

    if predict:
      name = os.path.basename(file)
      data.append([image,name])
    else:
      classs, name = file.split('/')[-2:]
      label = CLASS2LABEL[classs]#.astype(np.float32)
      #label = Classes2Labels(path)[classs]
      data.append([image,label,name,classs])

  # Return transpose of data so data[0] = images, data[1] = labels, etc.
  # instead of data[0] = [image0, label0, etc.]
  return np.array(data)#.T





### TODO: Normalize images?
# https://stats.stackexchange.com/questions/185853/why-do-we-need-to-normalize-the-images-before-we-put-them-into-cnn
# "The recommended preprocessing is to center the data to have mean of zero, 
# and normalize its scale to [-1, 1] along each feature"
# https://stats.stackexchange.com/questions/211436/why-do-we-normalize-images-by-subtracting-the-datasets-image-mean-and-not-the-c?rq=1
# I could scale and normalize here or do batch normalization in cnn
# https://en.wikipedia.org/wiki/Normalization_(image_processing)
def Normalize(I, Min=0., Max=255., newMin=-1., newMax=1.):
    I -= Min
    I *= (newMax - newMin)/(Max - Min)
    I += newMin
    return I



### Create two datasets -- train, valid -- of raw images and augmented images
#AUGMENTS = ['scaled', 'translated', 'rotated', 'flipped', 'gaussian']
def Datasets(path, size, validation_percent=20, \
            predict=False,
             augment=False, 
             augments=['scaled', 'translated', 'rotated', 'flipped', 'gaussian'],
             nimages=int(1e6), 
            nchannels=3):
  data = LoadImages(path, size, predict=predict, nimages=nimages, nchannels=nchannels)

  # Augment raw images to artificially increase statistics.
  # If N = instances in raw data, and M = instances in augmented data
  # M = N * (1 + nscalings + ntranslations + nrotations + nflips + ngaussians)
  # E.g. N = 271 --> M = 4336. That's a lot of (not necessarily good) statistics
  if augment:
    # Fill dictionary
    augdict = {'scaled': Augment(data, 'central_scale_images', size, [0.90, 0.75, 0.50, 0.25]),
               'translated': Augment(data, 'translate_images', size),
               'rotated': Augment(data, 'rotate_images', size),
               'flipped': Augment(data, 'flip_images', size),
               'gaussian': Augment(data, 'add_gaussian_noise', size),
                'perspective': Augment(data, 'perspective_transform', size)}

    if len(augments) == 1: augdata = augdict(augments)
    else: augdata = [augdict[a] for a in augments]
    augdata = np.array([item for sublist in augdata for item in sublist])

    data = np.concatenate([data, augdata])

  # Shuffle data in place
  np.random.shuffle(data)  

  # Split data into training and validation datasets
  # validation_percent is actual percent e.g. 10 not 0.10
  validation_size = int(round(data.shape[0]*validation_percent/100.))
  train = data[validation_size:]
  valid = data[:validation_size]

  return train, valid




def GetDatasets(path, size, \
                predict=False,
                augment=False,
                augments=['scaled', 'translated', 'rotated', 'flipped', 'gaussian'],
                nimages=int(1e6),
                nchannels=3,
                normalize=True):
  if augment:
    # https://stackoverflow.com/questions/5598181/python-multiple-prints-on-the-same-line
    print('\nLoading images and creating augmented images... ', end='', flush=True)
  else:
    print('\nLoading images... ', end='', flush=True)
  train, valid = Datasets(path, size, predict=predict, augment=augment, augments=augments,
                          nimages=nimages, nchannels=nchannels)
  print('Done.')
  if augment:
    print('Augments:', ', '.join(augments))

  # data[0] = [img0, label0, name0, class0], etc.
  #  --> imgs, labels, names, classes = data
  train = train.T
  valid = valid.T

  ### Print out number of training, validation images for each class
  print('Training images:  ', train.shape[1])
  #for i in range(len(train[1][0])):
  #  print(int(train[1].sum()[i]))
  classs, counts = np.unique(train[3], return_counts=True)
  for c, i in zip(classs, counts):
    print('                  ', c+':', i)
  print(  'Validation images:', valid.shape[1])
  classs, counts = np.unique(valid[3], return_counts=True)
  for c, i in zip(classs, counts):
    print('                  ', c+':', i)

  if normalize:
    # I forgot to normalize [0,255] --> [0,1]. Do that here.
    #train[0] = train[0]/255.#/train[0].max()
    #valid[0] = valid[0]/255.#/valid[0].max()
    train[0] = Normalize(train[0], newMin=0)
    valid[0] = Normalize(valid[0], newMin=0)

  return train, valid







