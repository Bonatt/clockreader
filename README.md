# Analogue Clock and Watch Reader


Given an image of an [anlogue clock or watch](https://en.wikipedia.org/wiki/Analog_watch) (future work: [digital clock or watch](https://en.wikipedia.org/wiki/Display_device#Segment_displays); arguably simpler on its own), determine time displayed by this clock or watch. Herewithin, "clock", "watch", etc., will refer to any [12-hour clock or watch](https://en.wikipedia.org/wiki/12-hour_clock) (future work: [24-hour clock](https://en.wikipedia.org/wiki/24-hour_clock)).


Assumptions:
1. Clock face is majority of image, i.e objected detection not required (future work: find any/all clocks within image)
2. Clock is only (two- or) three-handed, i.e. not a [chronograph](https://en.wikipedia.org/wiki/Chronograph).
3. Clock is orientated normally, i.e. not lying on its side.
4. Clock face and handset are simple in design, i.e. monochrome face and baton hands.

Regarding the two latter-most assumptions, these will be the first to be worked out after initial research, testing.


### Attempt One

5. Clock face is round, i.e. not square.

To identify the watch face. Below is the process from original image to identified watch face. This image was chosen for its simplicity (monochomaticity, straight-on perspective) while still retaining some complex features (numerals, strap). The image is loaded ~~, resized, and cropped to 512 x 512 pixels~~. From this image, edges are detected via [Canny operator](https://docs.opencv.org/4.0.1/dd/d1a/group__imgproc__feature.html#ga04723e007ed888ddf11d9ba04e2232de). From these edges, contours are found via [image moment and Green's theorem](https://docs.opencv.org/4.0.1/d3/dc0/group__imgproc__shape.html#gadf1ad6a0b82947fa1fe3c3d497f260e0). From these contours, ellipses are [fit via regression](https://docs.opencv.org/4.0.1/d3/dc0/group__imgproc__shape.html#gaf259efaad93098103d6c27b9e4900ffa). The two bottom-left images are without filtering, and the two bottom-right images are with filtering. The contours are filtered for those with areas `a` greater than 1% of the image area `A`, and the ellipses are filtered for those with [eccentricity](https://en.wikipedia.org/wiki/Eccentricity_(mathematics)) `e` less than 0.5. A [circularity](https://en.wikipedia.org/wiki/Shape_factor_(image_analysis_and_microscopy)\#Circularity) near 1 would suffice as well. For all ellipses, several significantly-similar ellipses were returned; the set of ellipses are reduced via `2*ceil(n/2)` where `n` is all parameters of the ellipse (center, semi-major and -minor axes, and rotation). Some other clustering algorithm, e.g., k-means, could also be employed. Note that due to figure output resolution, continuous lines may appear discontinuous.

![img](Plots/process0.png)

A single circle -- assumably the watch face -- is returned. Nice. Next step: identify watch hands within this watch face. Notice the largest ellipses in the bottom-left of the image above. The semi-major axis has potential for an orientation metric.


### Attempt Two

However, if the clockface was not round, or didn't exist at all, a human could still tell time. Therefore, the round shape is not important -- the hands are important. So what do we actually neeed to do to determine the time on an anlogue clock? Simple:

1. Locate hands
2. Determine which hands are which
3. Determine clock orientation
4. Calculate time

One could do this in several ways:

1. CV to find hands. Math to find time.
2. CV to find hands. ML to find time.
3. ML to find hands. Math to find time.
4. ML to find hands. ML to find time.


#### CV Approach

Until stated otherwise, all items below here are in flux (i.e. plots auto-generated, pushed, and possibly incorrect).

0. Edge detection via Canny operator.
1. [Morphological closing](https://en.wikipedia.org/wiki/Closing_(morphology)) to simplify thick, unusual hands if necessary hands (or other morphologies like skeletonization). The trick here is autotuning parameters for any image/hand shape, size...
3. Line (segment) detection via (Probabilistic) Hough transform.
4. Filter line segments for line location, etc. to reduce all lines to those that must be hands. ML techniques work well here.
4. Line intersection detection to determine central axis of hands.
5. Clustering to reduce lines to expected number of hands (3, or 2).
5. Calculate angle between hands (and orientation axis).
6. Determine which hands are which via length, area, etc.
6. Determine possible times from specific hand positions.

<!---
Below visualizes the approach above. The hands were correctly identified, and time correctly calculated.
![img](Plots/closed0.png)
-->
![img](Plots/time0.png)
![img](Plots/time1.png)

Note 1: The above may be wrong because it's from a working directory. I promise it was perfectly accurate for several methods. But during the adventure and quest for robustness amongst five (5) other images, methodologies have changed.

Note 2: Note to self for later (2019-02-28): Find HoughP line segments. Draw circles with center as one end point with radius equal to segment length. Repeat second (final) end point of segment. Repeat for all found lines. Then do math/HoughCircle to kind overlappin circles. Since clock hands converge at ~single point, ~three+ circles should be overlapping, or within eachother. Attempt this method for finding hands more robustly. Update (2019-03-05): I am currently still implementing this.
