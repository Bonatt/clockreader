import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import *

from config import *
from load import *

import cv2 as cv
import numpy as np
import imutils
from itertools import combinations


files = GetAllFiles(PATH)
f = files[2]

# 1. Load single image
# 2. Resize to height=SIZE (keeping aspect ration)
# 3. Trim any extra by cropping from center
im3 = cv.imread(f)
#im = imutils.resize(im, height=SIZE)
#im = CenterCrop(im, SIZE)
#im = CenterCrop(imutils.resize(cv.imread(f, 0), height=SIZE), SIZE)
im = cv.cvtColor(im3, cv.COLOR_BGR2GRAY) # "3" for 3 channels
img = cv.GaussianBlur(im, (5,5), 0) 


### Blank (black) images for masking.
blank = np.full(im.shape, 0, dtype=np.uint8)
blank3 = np.full(im3.shape, 0, dtype=np.uint8)


### Edges from im
edges = Canny(img)
edges3 = cv.cvtColor(edges.copy(), cv.COLOR_GRAY2BGR)


### Morphology
kernel = np.ones((5,5))
closed = cv.morphologyEx(edges, cv.MORPH_CLOSE, kernel)
closed3 = cv.cvtColor(closed.copy(), cv.COLOR_GRAY2BGR)


### Skeletonize
skel = imutils.skeletonize(closed, size=(3,3))
skel3 = cv.cvtColor(skel.copy(), cv.COLOR_GRAY2BGR)

def LengthGreaterThan(l, r=10):
    l = l[0]
    return True if np.sqrt((l[0]-l[2])**2+(l[1]-l[3])**2) > r else False

### Remove redundant lines by removing lines that are very similar
def reduceLines(lines, base=10):
    #return np.array([np.array([i]) for i in np.ceil(lines[:,0]/base)*base]).astype(int)
    lines2 = {tuple(i) for i in np.ceil(lines[:,0]/base)*base}
    return np.array([np.array([i]) for i in lines2])


### Probabilistic Hough Lines
LinesP = blank3.copy()
linesP = cv.HoughLinesP(closed.copy(), rho=1, theta=np.pi/180, threshold=50, minLineLength=SIZE/10, maxLineGap=1)
for line in linesP:
    cv.line(LinesP, (line[0][0], line[0][1]), (line[0][2], line[0][3]), GREEN, 1, cv.LINE_AA)

#pltimshow(np.hstack([im3, closed3, LinesP]))



### Skeletonize thick HoughP lines to get single lines. Re-HoughP with thin lines
'''
LinesP1 = cv.cvtColor(LinesP.copy(), cv.COLOR_BGR2GRAY)
LinesP1_skel = imutils.skeletonize(LinesP1.copy(), size=(3,3))
LinesP1_skel3 = cv.cvtColor(LinesP1_skel.copy(), cv.COLOR_GRAY2BGR)

LinesP2 = blank3.copy()
linesP2 = cv.HoughLinesP(LinesP1_skel.copy(), rho=1, theta=np.pi/180, threshold=50, minLineLength=SIZE/10,maxLineGap=1)
for line in linesP2:
    cv.line(LinesP2, (line[0][0], line[0][1]), (line[0][2], line[0][3]), GREEN, 1, cv.LINE_AA)
pltimshow(np.hstack([LinesP, LinesP1_skel3, LinesP2]))
'''

'''
for i,line in enumerate(linesP):
    line = line[0]
    m = (line[3]-line[1])/(line[2]-line[0])
    print(m)

for i,j in zip(linesP[:-1], linesP[1:]):
    i = i[0]
    j = j[0]
    mi = (i[3]-i[1])/(i[2]-i[0])
    mj = (j[3]-j[1])/(j[2]-j[0])
    if abs(mj - mi) < 3:
        print(i,j)
'''








lines = linesP[:,0]

def getSlopeIntercepts(lines):
    '''Takes array([[x1, y1, x2, y2], ...]), returns array([[m1, b1], ...])'''
    slopes = (lines[:,3]-lines[:,1])/(lines[:,2]-lines[:,0])
    intercepts = lines[:,1]-slopes*lines[:,0]
    return np.array([slopes, intercepts]).T 
    
mb = getSlopeIntercepts(lines)



def getIntersections(line1, line2):
    '''https://stackoverflow.com/a/42727584'''
    #m = np.array([line1, line2, np.ones(4)]).T
    m = np.hstack([np.reshape(np.array([line1, line2]), (4,2)), np.ones((4,1))])
    l1 = np.cross(m[0], m[1])
    l2 = np.cross(m[2], m[3])
    x, y, z = np.cross(l1, l2)
    if not z:
        return float('inf'), float('inf')
    return np.array([x/z, y/z])

def filterIntersections(intersections, sigma=1):
    mean = intersections.mean(0)
    std = intersections.std(0)
    se = std/np.sqrt(intersections.shape[1])
    zscores = (intersections-mean)/se
    return intersections[np.abs(zscores).sum(1) < sigma]
    

intersections0 = np.array([getIntersections(i,j) for i,j in combinations(lines,2)])
#intersections = intersections0[np.abs(intersections0.sum(1)) < max(im.shape)]#2*SIZE]

#zscores = (intersections-intersections.mean(0))/(intersections.std(0)/np.sqrt(intersections.shape[1]))
#zscores = zscores[(np.abs(zscores[:,0]) < 1) & (np.abs(zscores[:,1]) < 1)]
mean = intersections0.mean(0)
std = intersections0.std(0)
se = std/np.sqrt(intersections0.shape[1])
zscores = (intersections0-mean)/se

# What is the significance of filtering twice?
intersections = filterIntersections(intersections0)
intersections = filterIntersections(intersections)

center = intersections.mean(axis=0)
center_min = intersections.min(axis=0)
center_max = intersections.max(axis=0)
x,y = center.astype(int)
x_min, y_min = center_min.astype(int)
x_max, y_max = center_max.astype(int)

Center = blank3.copy()
#Center[:,x] = Center[y,:] = pltRED
#Center[:,x_min] = Center[y_min,:] = pltBLUE
#Center[:,x_max] = Center[y_max,:] = pltBLUE
for i,j in intersections.astype(int):
    Center[j,i] = 255
Center[y,x] = pltRED
pltimshow(LinesP+Center)



a1, a2, b1, b2 = (0, 1), (1, 2), (0, 10), (1, 9)
s = np.vstack([a1,a2,b1,b2])        # s for stacked
h = np.hstack((s, np.ones((4, 1)))) # h for homogeneous
l1 = np.cross(h[0], h[1])           # get first line
l2 = np.cross(h[2], h[3])           # get second line
x, y, z = np.cross(l1, l2)

line1, line2 = lines[0], lines[1]
m = np.array([line1, line2, np.ones(4)]).T
ll1 = np.cross(m[0], m[1])
ll2 = np.cross(m[2], m[3])
x, y, z = np.cross(ll1, ll2)




#intersections = [(x,y) for x,y in intersections if abs(x) < SIZE and abs(y) < SIZE]
#intersections = intersections[np.abs((intersections[:,0]) < SIZE) &
#                              (np.abs(intersections[:,1]) < SIZE)]
#center = np.array(intersections).mean(axis=0)













vectors = np.array([(x2-x1, y2-y1) for x1,y1,x2,y2 in linesP[:,0]])
magnitudes = [np.sqrt(x**2 + y**2) for x,y in vectors]
unit_vectors = [(tuple(v/m), e) for e,(v,m) in enumerate(zip(vectors, magnitudes))]





'''
y2,x2,y1,x1 = lines.T
plt.scatter(x1,y1)
plt.scatter(x2,y2)
plt.show()
'''


slopes = (lines[:,3]-lines[:,1])/(lines[:,2]-lines[:,0])
#slopes = sorted([((y2-y1)/(x2-x1),e) for e,(x1,y1,x2,y2) in enumerate(linesP[:,0])])
dots = sorted([(i[0]*i[2]+i[1]*i[3],e) for e,i in enumerate(linesP[:,0])])
'''
Slopes = []
for i,j in zip(slopes[:-1], slopes[1:]):
    print(i,j)
    if abs(i[0]-j[0]) < 3:
       Slopes.append((linesP[i[1]]+linesP[j[1]])/2)
'''

sames = []
slopes_combos = combinations(slopes,2)
for (s1,e1),(s2,e2) in slopes_combos:
    if abs(s1-s2) < 1:
        sames.append((e1,e2))




slopes_combos = combinations(slopes,2)
sames = {}
groups = range(3)
for (s1,e1),(s2,e2) in slopes_combos:
    if abs(s1-s2) < 1:
        
        if e1 not in sames:
            sames[e1] = []
        sames[e1].append(e2)



'''
def mergeLines(lines, d=3):
    Slopes = []
    for i,j in zip(slopes[:-1], slopes[1:]):
        print(i,j)
        if abs(i[0]-j[0]) < 3:
           Slopes.append(i)#(linesP[i[1]]+linesP[j[1]])/2)
    if len(Slopes) > 3:
        Slopes = mergeLines(Slopes)
    else:
        return Slopes
'''





vectors = [(x2-x1, y2-y1) for x1,y1,x2,y2 in linesP[:,0]]
magnitudes = [np.sqrt(x**2 + y**2) for x,y in vectors]
unit_vectors = [(tuple(v/m), e) for e,(v,m) in enumerate(zip(vectors, magnitudes))]

# I would do this in single loop, but I want the magnitudes
unit_vectors = [(tuple((x2-x1, y2-y1)/np.sqrt((x2-x1)**2+(y2-y1)**2)), e) for \
                e,(x1,y1,x2,y2) in enumerate(linesP[:,0])]


u = np.array(unit_vectors)
uT = u.T




unit_vector_combos = list(combinations(unit_vectors, 2))
#for ((x1,y1),e1), ((x2,y2),e2) in unit_vector_combos:
#    print(x1)

#plt.scatter(*np.array(vectors).T); plt.show(catter(*np.array(vectors).T); plt.show()


#Cos = []
Theta = []
line_combos = combinations(linesP[:,0],2)
for (u_x1, u_y1, u_x2, u_y2), (v_x1, v_y1, v_x2, v_y2) in line_combos:
    u = u_x2 - u_x1, u_y2 - u_y1
    v = v_x2 - v_x1, v_y2 - v_y1
    mu = np.sqrt(u[0]**2 + u[1]**2)
    mv = np.sqrt(v[0]**2 + v[1]**2)
    #cos = np.dot(u,v) / np.dot(mu, mv)
    #Cos.append(cos)
    theta = np.arccos(np.dot(u,v) / np.dot(mu, mv))
    Theta.append(theta)
#Cos = np.array(Cos)
for i in sorted(Theta): print(i)
