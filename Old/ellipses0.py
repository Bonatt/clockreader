### I forgot that I created some loaders, etc. while learning TF. Use those.
import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import * #pltimshow, cvimshow, Canny

from config import *
from load import * #GetDatasets
#from display import ImageGrid

import tensorflow as tf
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import imutils

#data = LoadImages(PATH, SIZE, nchannels=NCHANNELS, predict=True)
##data = GetDatasets(PATH, SIZE, predict=True, normalize=True)
#img = data[0][0]


### My loader isn't doing what I want so I will just manually do it here.
files = GetAllFiles(PATH)
f = files[1]

### Load single image
img = cv.imread(f, cv.IMREAD_GRAYSCALE)

### Look at different resizing functions
img1 = cv.resize(img, (SIZE, SIZE))
img2 = cvresize(img, height=SIZE)
img3 = cvresize2(img, SIZE)
img4 = imutils.resize(img, height=SIZE)
hline = np.full((SIZE,1), 0)
imgs = np.hstack([img1, hline, img2, hline, img3, hline, img4])
#pltimshow(imgs)
#cv.imwrite('resizes.png', imgs)

# Use only good one from above, and save color copy for plotting w/ color
img = img4.copy()
img_color = cv.cvtColor(img, cv.COLOR_GRAY2BGR)



### Detect edge (blurred first)
kernel = 5
sigma = 1
img_edges = Canny(img, blur=True, kernel=kernel, sigma=sigma)
img_edges_color = cv.cvtColor(img_edges, cv.COLOR_GRAY2BGR)

### Get contours from edges
#img_contours, # findContours() used to also return new image, but I pip 
# installed opencv-contrib-python and now it doesn't?
contours, hierarchy = cv.findContours(img_edges, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
contours2 = [i for i in contours if cv.contourArea(i) > int((SIZE*SIZE)*0.01)]
#contours = contours2.copy()
### Get contour from largest (maximum area) contour
'''
# doing weird to get biggest ellipse
max_area = max(cv.contourArea(c) for c in contours)
max_contour = [c for c in contours if cv.contourArea(c) == max_area][0]
img_contour_color = cv.drawContours(img_edges_color.copy(), max_contour, -1, GREEN)
### Get ellipse from largest contour
ellipse = cv.fitEllipse(max_contour)
img_ellipse = cv.ellipse(img_contour_color.copy(), ellipse, cvRED)
img_ellipse2 = cv.ellipse(img_color.copy(), ellipse, cvRED)
img_ellipse_color = img_ellipse.copy() #cv.cvtColor(i_ellipse, cv.COLOR_GRAY2RGB)
img_ellipse_color2 = img_ellipse2.copy()
'''
### Get all contours, and all ellipses
img_contours_color = cv.drawContours(img_edges_color.copy(), contours, -1, GREEN)
ellipses = [cv.fitEllipse(i) for i in contours]

img_ellipses_color = img_contours_color.copy()
for e in ellipses:
    cv.ellipse(img_ellipses_color, e, pltRED)
#pltimshow(img_ellipses)


#pltimshow(np.hstack([Canny(i), Canny(cv.GaussianBlur(i, (5,5), 2))]))
#img2 = np.hstack([img_color, img_edges_color, img_contours_color, img_ellipses_color, img_ellipse_color2])
img2 = np.vstack([img_color, img_edges_color, img_contours_color, img_ellipses_color])
#pltimshow(img2)
#cvimshow(img2, s=0)
#cv.imwrite('process.png', img2)




### Let's try filtering contours prior to finding ellipses, then filter ellipses
def getEllipses(edges, contourArea=SIZE*SIZE*0.01, eccentricity=0.9, 
                contourColor=pltGREEN, ellipseColor=pltRED):
    '''contourArea=0 & eccentricity=1. for all ellipses'''

    ellipses = []
    ellipseMask = np.zeros(edges.shape, dtype=np.uint8)
    
    contours, hierarchy = cv.findContours(edges, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
    #contoursMask = ellipseMask.copy()
    contours2 = []

    print(sum(cv.isContourConvex(i) for i in contours)) 

    for contour in contours:
        if cv.contourArea(contour) > contourArea:       
            ellipse = cv.fitEllipse(contour)
            #cx, cy = ellipse[0] # x, y coordinates of center
            b, a = ellipse[1] # minor, major semi-axes
            b /= 2 # Diameter to radius
            a /= 2
            #theta = ellipse[2] # rotation
            #print(b, a, np.sqrt(1-(b/a)**2))
            if np.sqrt(1-(b/a)**2) < eccentricity:
                ellipses.append(ellipse)
                cv.ellipse(ellipseMask, ellipse, ellipseColor)
                contours2.append(contour)
                #cv.drawContours(contoursMask, contour, -1, contourColor)

    return ellipses, ellipseMask, contours2 #, contoursMask



a, b, c = getEllipses(img_edges)



def filterEllipses(ellipses, eccentricity=0.5):
    ellipses_filtered = []
    for ellipse in ellipses:
        cx, cy = ellipse[0] # x, y coordinates of center
        a, b = ellipse[1] # major, minor semi-axes
        theta = ellipse[2] # rotation
        if np.sqrt(1-b**2/a**2) < eccentricity:
            ellipses_filtered.append(ellipse)
    return ellipses_filtered
        







### Circle Hough Transform
'''
img_hough = img_color.copy()

circles = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, SIZE, 
                          param1=50, param2=30, 
                          minRadius=0, maxRadius=0)
circles = np.round(circles).astype(int)

for c in circles[0]:
    cv.circle(img_hough, (c[0], c[1]), c[2], pltGREEN, 2)
    #cv.circle(img_hough, (c[0], c[1]), 2, pltRED, 3)

img3 = np.hstack([img2, img_hough])

pltimshow(img3)
'''



def findEllipses(edges):
    contours, _ = cv.findContours(edges.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    ellipseMask = np.zeros(edges.shape, dtype=np.uint8)
    contourMask = np.zeros(edges.shape, dtype=np.uint8)

    pi_4 = np.pi * 4

    for i, contour in enumerate(contours):
        if len(contour) < 5:
            continue

        area = cv.contourArea(contour)
        if area <= 100:  # skip ellipses smaller then 10x10
            continue

        arclen = cv.arcLength(contour, True)
        circularity = (pi_4 * area) / (arclen * arclen)
        ellipse = cv.fitEllipse(contour)
        poly = cv.ellipse2Poly((int(ellipse[0][0]), int(ellipse[0][1])), (int(ellipse[1][0] / 2), int(ellipse[1][1] / 2)), int(ellipse[2]), 0, 360, 5)

        # if contour is circular enough
        if circularity > 0.6:
            cv.fillPoly(ellipseMask, [poly], 255)
            continue

        # if contour has enough similarity to an ellipse
        similarity = cv.matchShapes(poly.reshape((poly.shape[0], 1, poly.shape[1])), contour, cv.CV_CONTOURS_MATCH_I2, 0)
        if similarity <= 0.2:
            cv.fillPoly(contourMask, [poly], 255)

    return ellipseMask, contourMask 
