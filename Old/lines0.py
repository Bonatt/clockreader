import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import *

from config import *
from load import *

import cv2 as cv
import numpy as np
import imutils



files = GetAllFiles(PATH)
f = files[2]

# 1. Load single image
# 2. Resize to height=SIZE (keeping aspect ration)
# 3. Trim any extra by cropping from center
im3 = cv.imread(f)
#im = imutils.resize(im, height=SIZE)
#im = CenterCrop(im, SIZE)
#im = CenterCrop(imutils.resize(cv.imread(f, 0), height=SIZE), SIZE)
im = cv.cvtColor(im3, cv.COLOR_BGR2GRAY) # "3" for 3 channels
imb = cv.bilateralFilter(im, 5, 100, 100)
img = cv.GaussianBlur(im, (5,5), 0) 
imm = cv.medianBlur(im, 9)

img3 = cv.GaussianBlur(im, (3,3), 0)
img5 = cv.GaussianBlur(im, (5,5), 0)
dog = img3*-1*img5



#ret, thresh = cv.threshold(im, 0, 255, cv.THRESH_OTSU)
thresh = cv.adaptiveThreshold(im, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 3)
connectivity = 4
nlabels, labels, stats, centroids = cv.connectedComponentsWithStats(thresh , connectivity, cv.CV_32S)
#pltimshow(labels)

### Blank (black) images for masking.
blank = np.full(im.shape, 0, dtype=np.uint8)
blank3 = np.full(im3.shape, 0, dtype=np.uint8)

### Edges from im
#edges = Canny(im, blur=True, blur_type='gaussian', kernel=3, sigma=1)
#edges = Canny(im, blur=True, blur_type='bilateral', sigmaColor=150, sigmaSpace=150)
edges = Canny(img)
edges3 = cv.cvtColor(edges.copy(), cv.COLOR_GRAY2BGR)

#pltimshow(np.hstack([im, imb, edges]))
#pltimshow(np.hstack([edges, cv.morphologyEx(im, cv.MORPH_GRADIENT, kernel)]))


### Feature detection
fd = cv.xfeatures2d.SIFT_create()
kp, desc = fd.detectAndCompute(im, None)
keypoints = cv.drawKeypoints(im, kp, None, GREEN, cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
#pltimshow(keypoints)


### Contours from edges
#contours, hierarchy = cv.findContours(edges.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
contours, hieratchy = cv.findContours(edges.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
contours3 = cv.drawContours(edges3.copy(), contours, -1, GREEN, thickness=cv.FILLED)
stack = np.hstack([edges3, contours3])
#pltimshow(stack)
#pltimshow(contours3)


### Morphology
kernel = np.ones((5,5))
#g = cv.getGaussianKernel(2,0)
#kernel = g * g.T
#cv.normalize(g*g.T, g*g.T, 1, 0, cv.NORM_MINMAX)
dilated = cv.dilate(edges, kernel, 1)
eroded = cv.erode(edges, kernel, 1)
opened = cv.morphologyEx(edges, cv.MORPH_OPEN, kernel)
closed = cv.morphologyEx(edges, cv.MORPH_CLOSE, kernel)
gradient = cv.morphologyEx(edges, cv.MORPH_GRADIENT, kernel)

#b = cv.adaptiveThreshold(im, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2)
#c = cv.morphologyEx(b, cv.MORPH_CLOSE, kernel)
#pltimshow(cv.morphologyEx(b, cv.MORPH_CLOSE, np.ones((2,2))))

### Contours morphed edges
closed3 = cv.cvtColor(closed.copy(), cv.COLOR_GRAY2BGR)
contours, hierarchy = cv.findContours(closed.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
contours3 = cv.drawContours(closed3.copy(), contours, -1, pltRED, thickness=cv.FILLED)


### Skeletonize
skel = imutils.skeletonize(closed, size=(3,3))
#pltimshow(np.hstack([closed, skel]))




def LengthGreaterThan(l, r=10):
    l = l[0]
    return True if np.sqrt((l[0]-l[2])**2+(l[1]-l[3])**2) > r else False

### Remove redundant lines by removing lines that are very similar
def reduceLines(lines, base=10):
    lines2 = np.array([np.array([i]) for i in np.ceil(lines[:,0]/base)*base]).astype(int)
    #lines2 = {tuple(i) for i in np.ceil(lines[:,0]/base)*base}
    #lines2 = np.array([np.array([i]) for i in lines2])
    return lines2




### Lines from closed
Lines0 = blank3.copy()
lines0 = cv.HoughLines(closed.copy(), rho=1, theta=np.pi/180, threshold=100)#, 0, 0, 0)
for line in lines0:
    rho = line[0][0]
    theta = line[0][1]
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a * rho
    y0 = b * rho
    f = 1000
    p1 = int(x0 - f*b), int(y0 + f*a)
    p2 = int(x0 + f*b), int(y0 - f*a)
    cv.line(Lines0, p1, p2, pltBLUE, 1, cv.LINE_AA)

### Probabilistic Hough
LinesP = blank3.copy()
linesP = cv.HoughLinesP(closed.copy(), rho=1, theta=np.pi/180, threshold=50, minLineLength=SIZE/10, maxLineGap=1)
for line in linesP:
    cv.line(LinesP, (line[0][0], line[0][1]), (line[0][2], line[0][3]), pltBLUE, 1, cv.LINE_AA)
#pltimshow(LinesP)

linesPr = reduceLines(linesP, 50)
LinesPr = blank3.copy()
for line in linesPr:
    cv.line(LinesPr, (line[0][0], line[0][1]), (line[0][2], line[0][3]), pltBLUE, 1, cv.LINE_AA)
pltimshow(np.hstack([LinesP,LinesPr]))


### Line Segment Detector
lsd = cv.createLineSegmentDetector()
lines, width, prec, nfa = lsd.detect(closed.copy())
Lines = lsd.drawSegments(blank.copy(), lines)
#pltimshow(Lines)
#pltimshow(np.hstack([Lines0, LinesP, Lines]))


#lines2 = np.array([l for l in lines if np.sqrt((l[0][0]-l[0][2])**2+(l[0][1]-l[0][3])**2) > 10])
#lengths = np.array([np.sqrt((l[0][0]-l[0][2])**2+(l[0][1]-l[0][3])**2) for l in lines])
lines2 = np.array([l for l in lines if LengthGreaterThan(l, SIZE/10)])
Lines2 = lsd.drawSegments(blank.copy(), lines2)
lines3 = reduceLines(lines2, 100)
Lines3 = lsd.drawSegments(blank.copy(), lines3)
#pltimshow(np.hstack([Lines0, LinesP, Lines,Lines2,Lines3]))




# HOG
hog = cv.HOGDescriptor()
h = hog.compute(img)









Lines2_g = cv.cvtColor(Lines2.copy(), cv.COLOR_BGR2GRAY)
