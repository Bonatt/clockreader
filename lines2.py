import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import *

from config import *
from load import *

import cv2 as cv
import numpy as np
import imutils
from itertools import combinations
from scipy.cluster.vq import kmeans
from scipy.cluster.hierarchy import dendrogram, linkage



# [Simple Watch Unbranded, Stock S001W, Seiko SKX009]
files = GetAllFiles(PATH)
filen = 1
f = files[filen]

### See https://stackoverflow.com/questions/37203970/opencv-grayscale-mode-vs-gray-color-conversion
# for problems in loading then typing, vs loading as type. I get three diff results for:
# 1. im3 = cv.imread(f); im = cv.cvtColor(im3, cv.COLOR_BGR2GRAY)
# 2. im = cv.imread(f, 0); im3 = cv.cvtColor(im, cv.COLOR_GRAY2BRG)
# 3. im3 = cv.imread(f); im = cv.cvtColor(im3, cv.COLOR_RGB2GRAY), etc.
# First method was used for creation of this code...

# 1. Load single image
# 2. Resize to height=SIZE (keeping aspect ration)
# 3. Trim any extra by cropping from center
im3 = cv.imread(f)
#im = imutils.resize(im, height=SIZE)
#im = CenterCrop(im, SIZE)
#im = CenterCrop(imutils.resize(cv.imread(f, 0), height=SIZE), SIZE)
im = cv.cvtColor(im3, cv.COLOR_BGR2GRAY) # "3" for 3 channels
img = cv.GaussianBlur(im, (5,5), 0) # "g" for gaussian blur 






### Blank (black) images for masking.
blank = np.full(im.shape, 0, dtype=np.uint8)
blank3 = np.full(im3.shape, 0, dtype=np.uint8)


### Edges from img
edges = Canny(img)
edges3 = cv.cvtColor(edges.copy(), cv.COLOR_GRAY2BGR)


### Morphology
kernel = np.ones((5,5))
closed = cv.morphologyEx(edges, cv.MORPH_CLOSE, kernel)
closed3 = cv.cvtColor(closed.copy(), cv.COLOR_GRAY2BGR)


### Skeletonize
#skel = imutils.skeletonize(closed, size=(3,3))
#skel3 = cv.cvtColor(skel.copy(), cv.COLOR_GRAY2BGR)


### Probabilistic Hough Lines
Lines = blank3.copy()
lines = cv.HoughLinesP(closed.copy(), rho=1, theta=np.pi/180, threshold=50, minLineLength=SIZE/10, maxLineGap=1)
lines = lines[:,0]
for x1,y1,x2,y2 in lines:
    cv.line(Lines, (x1, y1), (x2, y2), GREEN, 1, cv.LINE_AA)

stack = np.hstack([im3, closed3, Lines])
figsize = int(stack.shape[1]/100), int(stack.shape[0]/100)
d = 40
text = [(d/2, d, 'Original', 'k'),
        (im3.shape[1]+d/2, d, 'Closed', 'w'),
        (2*im3.shape[1]+d/2, d, 'Lines', 'g')]
pltimshow(stack, text=text, save=True, savename='Plots/closed{}.png'.format(filen), 
            figsize=figsize)



'''
### Vector analysis
vectors = np.array([(x2-x1, y2-y1) for x1,y1,x2,y2 in lines])

# K-means to cluster hands into three groups
hand_vectors, hand_distortion = kmeans(vectors.astype(float), 3)
hand_norms = np.linalg.norm(hand_vectors, axis=1)

# Assuming hand length = 1. second, 2. minute, 3. hour
hands = np.hstack([hand_norms.reshape(-1,1), hand_vectors])
hands.sort(axis=0) # This sorts BOTH columns???
hour, minute, second = hands

# Get angle of hand, rad
theta = np.arctan(hands[:,1]/hands[:,2])

# Assuming twelve-hour clock:
# hour = 1/12
# minute = 1/60 
# second = 1/60
time = theta*[12, 60, 60] + [12, 60, 60]
print(time)
'''





# K-means to cluster lines into three groups (hands)
lines0 = lines.copy()
lines, lines_distortion = kmeans(lines0.astype(float), 3)

def getIntersections(line1, line2):
    '''https://stackoverflow.com/a/42727584'''
    m = np.hstack([np.reshape(np.array([line1, line2]), (4,2)), np.ones((4,1))])
    l1 = np.cross(m[0], m[1])
    l2 = np.cross(m[2], m[3])
    x, y, z = np.cross(l1, l2)
    if not z:
        return float('inf'), float('inf')
    return np.array([x/z, y/z])

#center = getIntersections(lines1[0], lines1[1])
centers = np.array([getIntersections(i,j) for i,j in combinations(lines,2)])
center = centers.mean(axis=0).astype(int)

# Find L2 distances (Euclidean R) from center. Set nearest-three points to center 
# (note that points, lines are symbolically linked; changes to points will change 
# lines (sometimes?). Doing a .copy() would sever this. Get three furthest-most 
# points. Then normalize points to coord system centered on lines center,
# and, for completeness, recompute norms from center (lengths should increase 
# slightly)..
# https://stackoverflow.com/a/1401828; https://stackoverflow.com/a/2828121
points = lines.reshape(-1,2)
r = np.sqrt(np.sum((points-center)**2, axis=1))
points[r.argsort()[:3]] = center
points = points[r.argsort()][-3:].astype(int)
points -= center
r = np.sqrt(np.sum(points**2, axis=1)) 


Center = Lines.copy()
imCenter = im3.copy()
for x1,y1,x2,y2 in lines.astype(int):
    cv.line(Center, (x1, y1), (x2, y2), pltRED, 1, cv.LINE_AA)
    cv.line(imCenter, (x1, y1), (x2, y2), pltRED, 1, cv.LINE_AA)
for i,j in points+center:
    Center[j,i] = pltBLUE
Center[center[1], center[0]] = pltBLUE
#pltimshow(np.vstack([np.hstack([im3, closed3]), np.hstack([Lines, Center])]))
#pltimshow(Center)


### Working in degrees, transform point locations to time on a 12-hour clock.
# The lines have already been sorted by magnitude, and assuming the longest
# hand corresponds to seconds, and shortest to hours, we can extract in order.
# However, we may mistake the second for the minute hand, or vice versa.
# Redefine minute hand from more-reliable hour hand.
# https://en.wikipedia.org/wiki/Clock_angle_problem
# theta_hr = 0.5 * (60 * H + M) --> M = 2*theta_hr - 60 * H
# theta_min = 6 * M
theta_rad = np.arctan(points[:,1]/points[:,0])
theta_deg = theta_rad * 180/np.pi + 270
time = theta_deg / (30, 6, 6)
time[1] = 2*theta_deg[0] - 60 * int(time[0])
hour, minute, second = time
hour = int(hour)
minute = int(np.ceil(minute))
second = int(np.ceil(second))









title = '{}:{}:{} AM/PM'.format(hour, str(minute).zfill(2), str(second).zfill(2))
figsize = int(imCenter.shape[1]/100), int(imCenter.shape[0]/100)
pltimshow(imCenter, title=title,
            save=True, savename='Plots/time{}.png'.format(filen), 
            axes=False, figsize=figsize)
print(title)



'''
# Make new lines with center and new points 
#lines3 = np.hstack([np.tile(center,3).reshape(-1,2), points3])
#hand_vectors = points3-center
#hand_vectors = np.array([(x2-x1, y2-y1) for x1,y1,x2,y2 in lines3])
lines3 = np.hstack([np.zeros((3,2)),points-center])
hand_vectors = np.array([lines3[:,2]-lines3[:,0], lines3[:,3]-lines3[:,1]]).T
hand_norms = np.linalg.norm(hand_vectors, axis=1)

# Assuming hand length = 1. second, 2. minute, 3. hour
hands = np.hstack([hand_norms.reshape(-1,1), hand_vectors])
hands[hands[:,0].argsort()]
hour, minute, second = hands

# Get angle of hand, rad
theta = np.arctan(hands[:,1]/hands[:,2])

# Assuming twelve-hour clock:
# hour = 1/12
# minute = 1/60
# second = 1/60
time = theta*[12, 60, 60] + [12, 60, 60]
print(time)
'''











def LengthGreaterThan(l, r=10):
    l = l[0]
    return True if np.sqrt((l[0]-l[2])**2+(l[1]-l[3])**2) > r else False

### Skeletonize thick HoughP lines to get single lines. Re-HoughP with thin lines
'''
LinesP1 = cv.cvtColor(LinesP.copy(), cv.COLOR_BGR2GRAY)
LinesP1_skel = imutils.skeletonize(LinesP1.copy(), size=(3,3))
LinesP1_skel3 = cv.cvtColor(LinesP1_skel.copy(), cv.COLOR_GRAY2BGR)

LinesP2 = blank3.copy()
linesP2 = cv.HoughLinesP(LinesP1_skel.copy(), rho=1, theta=np.pi/180, threshold=50, minLineLength=SIZE/10,maxLineGap=1)
for line in linesP2:
    cv.line(LinesP2, (line[0][0], line[0][1]), (line[0][2], line[0][3]), GREEN, 1, cv.LINE_AA)
pltimshow(np.hstack([LinesP, LinesP1_skel3, LinesP2]))
'''

'''
for i,line in enumerate(linesP):
    line = line[0]
    m = (line[3]-line[1])/(line[2]-line[0])
    print(m)

for i,j in zip(linesP[:-1], linesP[1:]):
    i = i[0]
    j = j[0]
    mi = (i[3]-i[1])/(i[2]-i[0])
    mj = (j[3]-j[1])/(j[2]-j[0])
    if abs(mj - mi) < 3:
        print(i,j)
'''



def getSlopeIntercepts(lines):
    '''Takes array([[x1, y1, x2, y2], ...]), returns array([[m1, b1], ...])'''
    slopes = (lines[:,3]-lines[:,1])/(lines[:,2]-lines[:,0])
    intercepts = lines[:,1]-slopes*lines[:,0]
    return np.array([slopes, intercepts]).T 
    

def getIntersections(line1, line2):
    '''https://stackoverflow.com/a/42727584'''
    m = np.hstack([np.reshape(np.array([line1, line2]), (4,2)), np.ones((4,1))])
    l1 = np.cross(m[0], m[1])
    l2 = np.cross(m[2], m[3])
    x, y, z = np.cross(l1, l2)
    if not z:
        return float('inf'), float('inf')
    return np.array([x/z, y/z])

def filterIntersections(intersections, sigma=1):
    mean = intersections.mean(0)
    std = intersections.std(0)
    se = std/np.sqrt(intersections.shape[1])
    zscores = (intersections-mean)/se
    return intersections[np.abs(zscores).sum(1) < sigma]
    
'''
intersections0 = np.array([getIntersections(i,j) for i,j in combinations(lines,2)])
# What is the significance of filtering twice?
intersections = filterIntersections(intersections0)
intersections = filterIntersections(intersections)

center = intersections.mean(axis=0)
center_min = intersections.min(axis=0)
center_max = intersections.max(axis=0)
x,y = center.astype(int)
x_min, y_min = center_min.astype(int)
x_max, y_max = center_max.astype(int)

Center = blank3.copy()
#Center[:,x] = Center[y,:] = pltRED
#Center[:,x_min] = Center[y_min,:] = pltBLUE
#Center[:,x_max] = Center[y_max,:] = pltBLUE
for i,j in intersections.astype(int):
    Center[j,i] = 255
Center[y,x] = pltRED
pltimshow(Lines+Center)
'''








'''
norms = np.linalg.norm(vectors, axis=1)
unit_vectors = (vectors.T/norms).T # Silly transposing because shapes...
mb = getSlopeIntercepts(lines)

hand_vectors, hand_vectors_distortion = vq.kmeans(vectors.astype(float), 3)
hand_unit_vectors, hand_unit_vectors_distortion = vq.kmeans(unit_vectors.astype(float), 3)

plt.figure(figsize=(5,5))
#plt.plot(unit_vectors.T)#, label='u/|u|')
#plt.scatter(*unit_vectors.T)
plt.plot(hand_vectors.T)
plt.scatter(*hand_vectors.T)
plt.xlabel('x')
plt.ylabel('y')
plt.show()
'''


'''
#Cos = []
Theta = []
line_combos = combinations(lines,2)
for (u_x1, u_y1, u_x2, u_y2), (v_x1, v_y1, v_x2, v_y2) in line_combos:
    u = u_x2 - u_x1, u_y2 - u_y1
    v = v_x2 - v_x1, v_y2 - v_y1
    mu = np.sqrt(u[0]**2 + u[1]**2)
    mv = np.sqrt(v[0]**2 + v[1]**2)
    #cos = np.dot(u,v) / np.dot(mu, mv)
    #Cos.append(cos)
    theta = np.arccos(np.dot(u,v) / np.dot(mu, mv))
    Theta.append(theta)
#Cos = np.array(Cos)
for i in sorted(Theta): print(i)
'''
