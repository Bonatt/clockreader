import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import *

from config import *
from load import *

import cv2 as cv
import numpy as np
import imutils



files = GetAllFiles(PATH)
filen = 0
f = files[filen]

# 1. Load single image
# 2. Resize to height=SIZE (keeping aspect ration)
# 3. Trim any extra by cropping from center
#im = cv.imread(f, cv.IMREAD_GRAYSCALE)
#im = imutils.resize(im, height=SIZE)
#im = CenterCrop(im, SIZE)
#im = CenterCrop(imutils.resize(cv.imread(f, 0), height=SIZE), SIZE)
#im3 = cv.cvtColor(im, cv.COLOR_GRAY2BGR) # "3" for 3 channels

im3 = cv.imread(f) # "3" for 3 channels
im = cv.cvtColor(im3, cv.COLOR_BGR2GRAY)
img = cv.GaussianBlur(im, (5,5), 0) # "g" for gaussian blur


### Blank (black) images for masking.
blank = np.full(im.shape, 0, dtype=np.uint8)
blank3 = np.full(im3.shape, 0, dtype=np.uint8)


### Edges from im
edges = Canny(im, blur=True, kernel=3, sigma=1)
edges3 = cv.cvtColor(edges.copy(), cv.COLOR_GRAY2BGR)


### Contours from edges
contours, hierarchy = cv.findContours(edges, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
contours3 = cv.drawContours(blank3.copy(), contours, -1, GREEN)


### MinAreaRects (ellipses) from contours
mar = [cv.minAreaRect(i) for i in contours]
mar3 = blank3.copy()
for i in mar:
    cv.ellipse(mar3, i, GREEN)
#pltimshow(mar3)

### Ellipses from contours
ellipses = [cv.fitEllipse(i) for i in contours]
ellipses3 = blank3.copy()
for e in ellipses:
    cv.ellipse(ellipses3, e, pltRED)


stack = np.vstack([im3, edges3, contours3, ellipses3])
#pltimshow(stack)

pltimshow(np.hstack([im3, ellipses3, mar3]))




### Filter ellipses for only those with low eccentricity, < 1
# One could also filter for circularity ~1
def filterEllipses(ellipses, eccentricity=0.5):
    ellipses_filtered = []
    for ellipse in ellipses:
        cx, cy = ellipse[0] # x, y coordinates of center
        b, a = ellipse[1] # minor, major semi-axes
        theta = ellipse[2] # rotation
        if np.sqrt(1-(b/a)**2) < eccentricity:
            ellipses_filtered.append(ellipse)
    return ellipses_filtered

### Remove redundant ellipses by removing ellipese that are very similar
def reduceEllipses(ellipses, base=2, precision=0, debug=False):
    ellipses2 = []
    for ellipse in ellipses:
        cx, cy = ellipse[0] # x, y coordinates of center
        b, a = ellipse[1] # minor, major semi-axes
        theta = ellipse[2] # rotation
        #cx, cy, b, a, theta = np.round([cx, cy, b, a, theta], precision)
        #cx, cy, b, a, theta = np.floor([cx, cy, b, a, theta])
        if debug:
            print('cx,cy,b,a,theta={},{},{},{},{};f={}'.format(cx, cy, b, a, theta, (cx, cy, b, a, theta)))
        f = cx, cy, b, a, theta
        #cx, cy, b, a, theta = np.rint(base*np.round(np.array([cx, cy, b, a, theta])/base), precision)
        #cx, cy, b, a, theta = (base*(np.array(f)/base).round()).round(precision)
        cx, cy, b, a, theta = np.ceil(np.array(f)/base)*base
        ellipses2.append( ((cx,cy), (b,a), theta) )
    #ellipses2 = np.array(set(ellipses2))
    #return ellipses2
    return list(set(ellipses2))

'''
for i in ellipses2:
    print(i)
for i in reduceEllipses(ellipses2, debug=False):
    print(i)
'''

contours2 = [i for i in contours if cv.contourArea(i) > int((SIZE*SIZE)*0.01)]
contours32 = cv.drawContours(blank3.copy(), contours2, -1, GREEN)

ellipses2 = [cv.fitEllipse(i) for i in contours2]
ellipses2 = filterEllipses(ellipses2)
ellipses2 = reduceEllipses(ellipses2)

ellipses32 = blank3.copy()
for e in ellipses2:
    cv.ellipse(ellipses32, e, pltRED)


#t = contours32.copy()
#for e,c in zip(ellipses2, [pltGREEN, pltRED, pltBLUE, WHITE]):
#    cv.ellipse(t, e, c)



# Given image:
'''
1. find edges
2. find contours
3. filter for contours with:
3a. large area
3b. convex
4. filter for ellipses with:
4a. low eccentricity
4b. low diff between contour and ellipse
'''



'''
contour_list = []
for contour in contours:
    approx = cv.approxPolyDP(contour, 0.01*cv.arcLength(contour,True), True)
    area = cv.contourArea(contour)
    if ((len(approx) >= 20) & (area > SIZE*SIZE*0.01) ):
        contour_list.append(contour)
'''


### Plot just good ellipses, contours in second column of plot
box = np.full(im3.shape, 255, dtype=np.uint8)
stack2 = np.vstack([box, box, contours32, ellipses32])
fullstack = np.hstack([stack, stack2])
d = 50
text = [(d/2, d, 'Original', 'k'), 
        (d/2, SIZE+d, 'Edges', 'w'),
        (d/2, 2*SIZE+d, 'Contours', 'g'),
        (SIZE+d/2, 2*SIZE+d, 'Contours (a > 0.01*A)', 'g'),
        (d/2, 3*SIZE+d, 'Ellipses', 'r'),
        (SIZE+d/2, 3*SIZE+d, 'Ellipses (e > 0.5)', 'r')]
figsize = int(fullstack.shape[1]/100), int(fullstack.shape[0]/100)
pltimshow(fullstack, figsize=figsize, text=text, save=True, savename='Plots/process{}.png'.format(filen))
#cv.imwrite('process.png', fullstack)
