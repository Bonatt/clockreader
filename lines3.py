import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import *

from config import *
from load import *

import cv2 as cv
import numpy as np
import imutils
from itertools import combinations
from scipy.cluster.vq import kmeans
from scipy.cluster.hierarchy import dendrogram, linkage



# [Simple Watch Unbranded, Stock S001W, Seiko SKX009, Simple Two Hand, Urban Outfitters]
filen = 0
files = GetAllFiles(PATH)
f = files[filen]

### See https://stackoverflow.com/questions/37203970/opencv-grayscale-mode-vs-gray-color-conversion
# for problems in loading then typing, vs loading as type. I get three diff results for:
# 1. im3 = cv.imread(f); im = cv.cvtColor(im3, cv.COLOR_BGR2GRAY)
# 2. im = cv.imread(f, 0); im3 = cv.cvtColor(im, cv.COLOR_GRAY2BRG)
# 3. im3 = cv.imread(f); im = cv.cvtColor(im3, cv.COLOR_RGB2GRAY), etc.
# First method was used for creation of this code...

# 1. Load single image
# 2. Resize to height=SIZE (keeping aspect ration)
# 3. Trim any extra by cropping from center
im3 = cv.imread(f)
#im = imutils.resize(im, height=SIZE)
#im = CenterCrop(im, SIZE)
#im = CenterCrop(imutils.resize(cv.imread(f, 0), height=SIZE), SIZE)
im = cv.cvtColor(im3, cv.COLOR_BGR2GRAY) # "3" for 3 channels
img = cv.GaussianBlur(im, (5,5), 0) # "g" for gaussian blur 
imb = cv.bilateralFilter(im, d=5, sigmaColor=10, sigmaSpace=10)



### Blank (black) images for masking.
blank = np.full(im.shape, 0, dtype=np.uint8)
blank3 = np.full(im3.shape, 0, dtype=np.uint8)


### Edges from img
edges = Canny(img)
edges3 = cv.cvtColor(edges.copy(), cv.COLOR_GRAY2BGR)
edgesb = cv.GaussianBlur(edges, (5,5), 0)

### Contours from edges
'''
contours, hieratchy = cv.findContours(edges.copy(), cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
#contours3 = cv.drawContours(blank3.copy(), contours, -1, GREEN, -1)#thickness=cv.FILLED)
Contours = blank3.copy()
for i in contours:
    cv.drawContours(Contours, [i], -1, GREEN, -1)
    #cv.drawContours(Contours, [i[1]], -1, GREEN, -1)
Poly = blank3.copy()
for i in contours:
    cv.fillPoly(Poly, [i[:,0]], GREEN)
pltimshow(Poly)
'''

### Morphology
n = 5
kernel = np.ones((n,n))
closed = cv.morphologyEx(edges, cv.MORPH_CLOSE, kernel)
closed3 = cv.cvtColor(closed.copy(), cv.COLOR_GRAY2BGR)
blackhat = cv.morphologyEx(edges, cv.MORPH_BLACKHAT, kernel)
blackhatclosed = cv.morphologyEx(blackhat, cv.MORPH_CLOSE, kernel)
#pltimshow(np.hstack([edges, closed, blackhat, blackhatclosed]))


### Skeletonize
skel = imutils.skeletonize(closed, size=(3,3))
skel3 = cv.cvtColor(skel.copy(), cv.COLOR_GRAY2BGR)

# https://stackoverflow.com/a/15772251
from skimage import morphology
#im_normed = (im-im.min())/(im.max()-im.min())#cv.normalize(im, None, 0, 1, cv.NORM_MINMAX)
#skel2 = morphology.skeletonize(im_normed > 0).astype(float)
# Norm to 0-1 for sk, then norm back to 0-255 for plt, cv
closed_normed = cv.normalize(closed, None, 0, 1, cv.NORM_MINMAX)
skel2 = morphology.skeletonize(closed_normed).astype(float)
skel2 = cv.normalize(skel2, skel2, 0, 255, cv.NORM_MINMAX)
#pltimshow(np.hstack([skel, skel2]))





### Probabilistic Hough Lines
IFL = blackhatclosed.copy() # Image For Lines
#IFL = closed.copy()
#IFL = edges.copy()
IFL = cv.GaussianBlur(IFL.copy(), (3,3), 0)
IFL3 = cv.cvtColor(IFL.copy(), cv.COLOR_GRAY2BGR)

Lines = IFL3.copy() #blank3.copy()
rho = 1
theta = np.pi/180
minLineLength = 0.05 * np.sqrt(im.shape[0]**2 + im.shape[1]**2) #np.sqrt(im.shape[0]*im.shape[1])
maxLineGap = 3
threshold = int(minLineLength) + maxLineGap
lines = cv.HoughLinesP(IFL.copy(), rho=rho, theta=theta, threshold=threshold, 
                        minLineLength=minLineLength, maxLineGap=maxLineGap)
lines = lines[:,0]
lines0 = lines.copy()
for x1,y1,x2,y2 in lines:
    cv.line(Lines, (x1, y1), (x2, y2), pltRED, 1, cv.LINE_AA)
'''
stack = np.hstack([im3, closed3, Lines])
figsize = int(stack.shape[1]/100), int(stack.shape[0]/100)
d = 40
text = [(d/2, d, 'Original', 'k'),
        (im3.shape[1]+d/2, d, 'Closed', 'w'),
        (2*im3.shape[1]+d/2, d, 'Lines', 'g')]
pltimshow(stack, text=text, save=True, savename='Plots/closed{}.png'.format(filen), 
            figsize=figsize)
'''

### Filter lines by position. Lines at or very near edge of image are removed.
# Filter lines within p% of x, y. There must be a better way to code this...
# Remember (d'oh): a.shape = (y, x)
p = 0.1
minx, miny = p*im.shape[1], p*im.shape[0]
maxx, maxy = (1-p)*im.shape[1], (1-p)*im.shape[0]
padx1 = (minx<lines[:,0]) & (lines[:,0]<maxx)
padx2 = (minx<lines[:,2]) & (lines[:,2]<maxx)
pady1 = (miny<lines[:,1]) & (lines[:,1]<maxy)
pady2 = (miny<lines[:,3]) & (lines[:,3]<maxy)
lines = lines[padx1 & padx2 & pady1 & pady2]
lines00 = lines.copy()
for x1,y1,x2,y2 in lines:
    cv.line(Lines, (x1, y1), (x2, y2), pltBLUE, 1, cv.LINE_AA)



### I've increased minLineLenth from SIZE/10 to SIZE/100 to capture more lines.
# Now I want to do my own metric for chosing longer lines
# Get lines greater than index >= greatest slope
'''
lengths = np.sqrt((lines[:,2]-lines[:,0])**2 + (lines[:,3]-lines[:,1])**2)
lengths = list(zip(lengths, range(len(lengths))))
lengths.sort()
#lengthsr = [i for i,j in lengths]
#for i,length in enumerate(lengthsr[1:]):
#   print(lengthsr[i+1] + lengthsr[i-1] - 2*lengthsr[i])
dlengths = np.gradient([i for i,j in lengths])
ddlengths = abs(np.gradient(dlengths))
plt.plot([i for i,j in lengths])
plt.plot(dlengths)
plt.plot(ddlengths)
#plt.plot([i for i,j in lengths], dlengths)
plt.show()

lines = lines[[j for i,j in lengths[ddlengths.argmax():]]]
lines000 = lines.copy()
for x1,y1,x2,y2 in lines:
    cv.line(Lines, (x1, y1), (x2, y2), pltGREEN, 2, cv.LINE_AA)
'''
#pltimshow(Lines)


### Lines from closed
'''
LinesH = blank3.copy()
linesH = cv.HoughLines(closed.copy(), rho=1, theta=np.pi/180, threshold=100)#, 0, 0, 0)
for line in linesH:
    rho = line[0][0]
    theta = line[0][1]
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a * rho
    y0 = b * rho
    f = 1000
    p1 = int(x0 - f*b), int(y0 + f*a)
    p2 = int(x0 + f*b), int(y0 - f*a)
    cv.line(LinesH, p1, p2, GREEN, 1, cv.LINE_AA)
'''




# K-means to cluster lines into three groups (hands)
lines, lines_distortion = kmeans(lines.copy().astype(float), 3)
#lines, lines_distortion = kmeans(lines2.astype(float), 3)

'''
metric = 'ward'
z0 = linkage(lines0, metric)
plt.figure()
d0 = dendrogram(z0)
plt.show()
'''

def getIntersections(line1, line2):
    '''https://stackoverflow.com/a/42727584'''
    m = np.hstack([np.reshape(np.array([line1, line2]), (4,2)), np.ones((4,1))])
    l1 = np.cross(m[0], m[1])
    l2 = np.cross(m[2], m[3])
    x, y, z = np.cross(l1, l2)
    if not z:
        return np.array([np.inf, np.inf])
    return np.array([x/z, y/z])

centers = np.array([getIntersections(i,j) for i,j in combinations(lines,2)])
centers = centers[(0<centers[:,0]) & (0<centers[:,1]) & (centers[:,0]<im.shape[0]) & (centers[:,1]<im.shape[0])]
center = centers.mean(axis=0)



lines00 = lines.copy()
# Find L2 distances (Euclidean R) from center. Set nearest-three points to center 
# (note that points, lines are symbolically linked; changes to points will change 
# lines (sometimes?). Doing a .copy() would sever this. Get three furthest-most 
# points. Then normalize points to coord system centered on lines center.
# And, for completeness, recompute norms from center (lengths should increase 
# slightly).
# https://stackoverflow.com/a/1401828; https://stackoverflow.com/a/2828121
#'''
points = lines.reshape(-1,2)#.copy()
r = np.sqrt(np.sum((points-center)**2, axis=1))
points[r.argsort()[:3]] = center
points = points[r.argsort()][-3:]#.astype(int)
points -= center
r = np.sqrt(np.sum(points**2, axis=1)) 
'''
points = lines.reshape(-1,2) - center
r = np.sqrt(np.sum(points**2, axis=1))
points[r.argsort()[:3]] = 0
points = points[r.argsort()][-3:]
r = np.sqrt(np.sum(points**2, axis=1))
'''

# Set p1 to center (sometimes vectors are reverse direction and I'm unsure of
# OpenCV convenction regarding vectors)
# ...This did not fix my problem of minute hand in first quadrant always being mirrored
for i,(x1,y1,x2,y2) in enumerate(lines):
    if (x2, y2) == tuple(center):
        lines[i] = np.array([x2,y2,x1,y1])

Points = blank3.copy()
for x,y in (points+center).astype(int):
    Points[y,x] = 255



Center = blank3.copy()
imCenter = im3.copy()
for x1,y1,x2,y2 in lines.astype(int):
    cv.line(Center, (x1, y1), (x2, y2), pltRED, 1, cv.LINE_AA)
    cv.line(imCenter, (x1, y1), (x2, y2), pltRED, 1, cv.LINE_AA)
for x,y in (points+center).astype(int):
    Center[y,x] = pltBLUE
Center[int(center[1]), int(center[0])] = pltBLUE
#pltimshow(np.vstack([np.hstack([im3, closed3]), np.hstack([Lines, Center])]))
#pltimshow(Center)
stack = np.hstack([im3, IFL3, Center])
figsize = int(stack.shape[1]/100), int(stack.shape[0]/100)
d = 40
text = [(d/2, d, 'Original', 'k'),
        (im3.shape[1]+d/2, d, 'Processed', 'w'),
        (2*im3.shape[1]+d/2, d, 'Lines', 'g')]
#pltimshow(stack, text=text, figsize=figsize,
#            save=True, savename='Plots/processed{}.png'.format(filen))
#pltimshow(np.hstack([Lines, Center]))


### Working in degrees, transform point locations to time on a 12-hour clock.
# The lines have already been sorted by magnitude, and assuming the longest
# hand corresponds to seconds, and shortest to hours, we can extract in order.
# However, we may mistake the second for the minute hand, or vice versa.
# Redefine minute hand from more-reliable hour hand.
# https://en.wikipedia.org/wiki/Clock_angle_problem
# theta_hr = 0.5 * (60 * H + M) --> M = 2*theta_hr - 60 * H
# theta_min = 6 * M
#points = [[1,1],[-1,-1],[1,1]] * points
theta_rad = np.arctan(points[:,1]/points[:,0])
#np.arctan2(points[:,1], points[:,0]) Work with this instead
orientation = 0 # For future titled watches
theta_deg = theta_rad * 180/np.pi + 270 + orientation
time0 = theta_deg / (30, 6, 6)
time = time0.copy()
time[1] = 2*theta_deg[0] - 60 * int(time[0])
hour, minute, second = time
hour = int(hour)
minute = int(np.ceil(minute))
second = int(np.ceil(second))

title = '{}:{}:{} AM/PM'.format(hour, str(minute).zfill(2), str(second).zfill(2))
figsize = int(imCenter.shape[1]/100), int(imCenter.shape[0]/100)
pltimshow(imCenter, title=title,
            save=True, savename='Plots/time{}.png'.format(filen), 
            axes=False, figsize=figsize)
print(time0)
print(time)
print(title)





#plt.scatter(*points.T); plt.show()







def LengthGreaterThan(l, r=10):
    l = l[0]
    return True if np.sqrt((l[0]-l[2])**2+(l[1]-l[3])**2) > r else False

### Skeletonize thick HoughP lines to get single lines. Re-HoughP with thin lines
'''
LinesP1 = cv.cvtColor(LinesP.copy(), cv.COLOR_BGR2GRAY)
LinesP1_skel = imutils.skeletonize(LinesP1.copy(), size=(3,3))
LinesP1_skel3 = cv.cvtColor(LinesP1_skel.copy(), cv.COLOR_GRAY2BGR)

LinesP2 = blank3.copy()
linesP2 = cv.HoughLinesP(LinesP1_skel.copy(), rho=1, theta=np.pi/180, threshold=50, minLineLength=SIZE/10,maxLineGap=1)
for line in linesP2:
    cv.line(LinesP2, (line[0][0], line[0][1]), (line[0][2], line[0][3]), GREEN, 1, cv.LINE_AA)
pltimshow(np.hstack([LinesP, LinesP1_skel3, LinesP2]))
'''

'''
for i,line in enumerate(linesP):
    line = line[0]
    m = (line[3]-line[1])/(line[2]-line[0])
    print(m)

for i,j in zip(linesP[:-1], linesP[1:]):
    i = i[0]
    j = j[0]
    mi = (i[3]-i[1])/(i[2]-i[0])
    mj = (j[3]-j[1])/(j[2]-j[0])
    if abs(mj - mi) < 3:
        print(i,j)
'''



def getSlopeIntercepts(lines):
    '''Takes array([[x1, y1, x2, y2], ...]), returns array([[m1, b1], ...])'''
    slopes = (lines[:,3]-lines[:,1])/(lines[:,2]-lines[:,0])
    intercepts = lines[:,1]-slopes*lines[:,0]
    return np.array([slopes, intercepts]).T 
    

def filterIntersections(intersections, sigma=1):
    mean = intersections.mean(0)
    std = intersections.std(0)
    se = std/np.sqrt(intersections.shape[1])
    zscores = (intersections-mean)/se
    return intersections[np.abs(zscores).sum(1) < sigma]
    
'''
intersections0 = np.array([getIntersections(i,j) for i,j in combinations(lines,2)])
# What is the significance of filtering twice?
intersections = filterIntersections(intersections0)
intersections = filterIntersections(intersections)

center = intersections.mean(axis=0)
center_min = intersections.min(axis=0)
center_max = intersections.max(axis=0)
x,y = center.astype(int)
x_min, y_min = center_min.astype(int)
x_max, y_max = center_max.astype(int)

Center = blank3.copy()
#Center[:,x] = Center[y,:] = pltRED
#Center[:,x_min] = Center[y_min,:] = pltBLUE
#Center[:,x_max] = Center[y_max,:] = pltBLUE
for i,j in intersections.astype(int):
    Center[j,i] = 255
Center[y,x] = pltRED
pltimshow(Lines+Center)
'''








'''
norms = np.linalg.norm(vectors, axis=1)
unit_vectors = (vectors.T/norms).T # Silly transposing because shapes...
mb = getSlopeIntercepts(lines)

hand_vectors, hand_vectors_distortion = vq.kmeans(vectors.astype(float), 3)
hand_unit_vectors, hand_unit_vectors_distortion = vq.kmeans(unit_vectors.astype(float), 3)

plt.figure(figsize=(5,5))
#plt.plot(unit_vectors.T)#, label='u/|u|')
#plt.scatter(*unit_vectors.T)
plt.plot(hand_vectors.T)
plt.scatter(*hand_vectors.T)
plt.xlabel('x')
plt.ylabel('y')
plt.show()
'''


'''
#Cos = []
Theta = []
line_combos = combinations(lines,2)
for (u_x1, u_y1, u_x2, u_y2), (v_x1, v_y1, v_x2, v_y2) in line_combos:
    u = u_x2 - u_x1, u_y2 - u_y1
    v = v_x2 - v_x1, v_y2 - v_y1
    mu = np.sqrt(u[0]**2 + u[1]**2)
    mv = np.sqrt(v[0]**2 + v[1]**2)
    #cos = np.dot(u,v) / np.dot(mu, mv)
    #Cos.append(cos)
    theta = np.arccos(np.dot(u,v) / np.dot(mu, mv))
    Theta.append(theta)
#Cos = np.array(Cos)
for i in sorted(Theta): print(i)
'''
