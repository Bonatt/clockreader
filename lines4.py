import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import *

from config import *
from load import *

import cv2 as cv
import numpy as np
import imutils
from itertools import combinations
from scipy.cluster.vq import kmeans
from scipy.cluster.hierarchy import dendrogram, linkage




### Load Image. Do not resize.
# 0 Simple Watch Unbranded  
# 1 Stock S001W  
# 2 Seiko SKX009  
# 3 Urban Outfitters  
# 4 Simple Two Hand  (Deleted, oops)
filen = 1
files = GetAllFiles(PATH)
f = files[filen]
im3 = cv.imread(f) # "3" for 3 channels
im = cv.cvtColor(im3, cv.COLOR_BGR2GRAY)
# Show raw and b/w images, converting both to RGB prior to displaying.
pltimshow(np.hstack([cv.cvtColor(im3, cv.COLOR_BGR2RGB), 
                     cv.cvtColor(im, cv.COLOR_GRAY2BGR)]))


### Blank (black) images for masking.
blank = np.full(im.shape, 0, dtype=np.uint8)
blank3 = np.full(im3.shape, 0, dtype=np.uint8)


### Blur, Canny, BlurCanny
k = 5
kernelsize = (k,k)
kernel = np.ones(kernelsize)
imb = cv.GaussianBlur(im, kernelsize, 0) # "b" for blur
edges = Canny(imb)
edges3 = cv.cvtColor(edges.copy(), cv.COLOR_GRAY2BGR)
edgesb = cv.GaussianBlur(edges.copy(), kernelsize, 0)
edges3b = cv.cvtColor(edgesb.copy(), cv.COLOR_GRAY2BGR)
pltimshow(np.hstack([edges, edgesb]))


### Morphology
closed = cv.morphologyEx(edges, cv.MORPH_CLOSE, kernel)
closed3 = cv.cvtColor(closed.copy(), cv.COLOR_GRAY2BGR)
blackhat = cv.morphologyEx(edges, cv.MORPH_BLACKHAT, kernel)
blackhatclosed = cv.morphologyEx(blackhat, cv.MORPH_CLOSE, kernel)
pltimshow(np.hstack([edges, closed, blackhat, blackhatclosed]))



### Probabilistic Hough Transform
IFL = blackhatclosed.copy() # Image For Lines
IFL = cv.GaussianBlur(IFL.copy(), (3,3), 0)
IFL3 = cv.cvtColor(IFL.copy(), cv.COLOR_GRAY2BGR)

rho = 1
theta = np.pi/180
minLineLength = 0.05 * np.sqrt(im.shape[0]**2 + im.shape[1]**2)
maxLineGap = 3
threshold = int(minLineLength) + maxLineGap
lines = cv.HoughLinesP(IFL.copy(), rho=rho, theta=theta, threshold=threshold,
                       minLineLength=minLineLength, maxLineGap=maxLineGap)
# Redice dimensionality of lines variable, [[x,y,..]] --> [x,y,..].
lines = lines[:,0]

Lines = IFL3.copy()
for x1,y1,x2,y2 in lines:
    cv.line(Lines, (x1, y1), (x2, y2), pltRED, 1, cv.LINE_AA)
pltimshow(Lines)



### Filter lines by position. Lines at or very near edge of image are removed.
# Filter lines within p*100% of x, y. There must be a better way to code this...
# Remember (d'oh): a.shape = (y, x)
def inFrame(im, lines, p=0.2):
    minx, miny = p*im.shape[1], p*im.shape[0]
    maxx, maxy = (1-p)*im.shape[1], (1-p)*im.shape[0]
    padx1 = (minx<lines[:,0]) & (lines[:,0]<maxx)
    padx2 = (minx<lines[:,2]) & (lines[:,2]<maxx)
    pady1 = (miny<lines[:,1]) & (lines[:,1]<maxy)
    pady2 = (miny<lines[:,3]) & (lines[:,3]<maxy)

    lines = lines[padx1 & padx2 & pady1 & pady2]

    for x1,y1,x2,y2 in lines:
        cv.line(Lines, (x1, y1), (x2, y2), pltBLUE, 1, cv.LINE_AA)
    
    return lines, Lines

lines, Lines = inFrame(im, lines)
pltimshow(Lines)




### K-means to cluster lines into three groups (hands)
lines, lines_distortion = kmeans(lines.copy().astype(float), 3)




def getIntersections(line1, line2):
    '''https://stackoverflow.com/a/42727584'''
    m = np.hstack([np.reshape(np.array([line1, line2]), (4,2)), np.ones((4,1))])
    l1 = np.cross(m[0], m[1])
    l2 = np.cross(m[2], m[3])
    x, y, z = np.cross(l1, l2)
    if not z:
        return np.array([np.inf, np.inf])
    return np.array([x/z, y/z])

# Get intersections. Trim by intersections outside image.
centers = np.array([getIntersections(i,j) for i,j in combinations(lines,2)])
centers = centers[(0<centers[:,0]) & (0<centers[:,1]) & (centers[:,0]<im.shape[0]) & (centers[:,1]<im.shape[0])]
center = centers.mean(axis=0)





# Transform image-referenced lines (x1,y1,x2,y2) to zero-referenced individual coordinates (x,y).
# Find and sort by L2 norm. Set the three lines associated with the three coordinated with lowest 
# norm to line intersection center. Only keep the three coordinates and associated norms with 
# highest norm.
points = lines.reshape(-1,2) - center
r = np.sqrt(np.sum(points**2, axis=1))
lines.reshape(-1,2)[r.argsort()[:k]] = center
points = points[r.argsort()][-k:]
r = r[r.argsort()][-k:]




def NormalizePoints(points, recenter=False):
    '''Assumes points are of form np.array([[-55,-77], [50,-45], [-60,60]])
    and shortest-longest hands are of order (h, m, s) (array is not necessarily 
    in order, however). Takes points, returns sorted points with reference 
    point as center of points'''
    
    if recenter:
        points = points - points.mean(axis=0)
        center = np.array([0,0])
    else:
        center = points.mean(axis=0)
    
    # Order hands shortest-longest == (h, m, s) via L2 norm.
    r = np.sqrt(np.sum((points-center)**2, axis=1))
    points = points[r.argsort()]
    
    return points, center, r


def Points2Time(points, recenter=False, orientation=0):
    '''Assumes points are of form np.array([[-55,-77], [50,-45], [-60,60]])
    and shortest-longest hands are of order (h, m, s) (array is not 
    necessarily in order, however).'''
    
    points, center, r = NormalizePoints(points, recenter=recenter)
    
    theta_rad = np.arctan(points[:,1]/points[:,0])
    theta_rad2 = np.arctan2(points[:,1], points[:,0])
    
    # +270 degrees for math --> clock convention
    theta_deg = theta_rad * 180/np.pi + 270 + orientation
    
    # Rotate theta[i] 180 degrees if sign(atan[i]) == sign(atan2[i]).
    # Modulo 360 for overflow. I could -1*x, y points, but adding
    # 180 degrees to appropriate thetas works fine here. 
    arctans_are_same = np.sign(theta_rad) == np.sign(theta_rad2) 
    rotate180 = arctans_are_same * 180

    theta_deg = (theta_deg + rotate180)%360
    print(len(theta_deg))
    if len(theta_deg) == 2:
        time = theta_deg / (30, 6)
        hour, minute = time.astype(int)
        second = 0
    elif len(theta_deg) == 3:
        time = theta_deg / (30, 6, 6)
        hour, minute, second = time.astype(int)
        
    #if not hour: hour = 12
    time_str = '{0:02}:{1:02}:{2:02} AM/PM'.format(hour, minute, second)
    
    return time, time_str



if filen == 0: orientation = 0
elif filen == 1: orientation = 90
elif filen == 2: orientation = 30
else: orientation = 0
time, time_str = Points2Time(points, orientation=orientation)


imLines = cv.cvtColor(im, cv.COLOR_GRAY2BGR).copy()
for x1,y1,x2,y2 in lines.astype(int):
    cv.line(imLines, (x1, y1), (x2, y2), GREEN, 1, cv.LINE_AA)
pltimshow(imLines, title=time_str, axes=False)
